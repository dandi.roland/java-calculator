package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //Initial Scanner to read user's input
        Scanner sc = new Scanner(System.in);

        //Read first number
        System.out.print("Please enter the first number: ");
        double firstNum = sc.nextDouble();
        sc.nextLine();

        //Read operator (A - Addition, S - Subtraction)
        System.out.printf("Operators Options: %n A - Addition %n S - Subtraction %n");
        System.out.print("Please enter operator: ");
        String operator = sc.nextLine();

        //Read second number
        System.out.print("Please enter the second number: ");
        double secondNum = sc.nextDouble();
        sc.nextLine();

        //Print all input
        System.out.println("First number: " + firstNum);
        System.out.println("Operator: " + operator);
        System.out.println("Second number: " + secondNum);

        //Calculation logic
        switch (operator) {
            case "A": // Addition
                System.out.printf("Result of %s add %s is %s %n", firstNum, secondNum, (firstNum + secondNum));
                break;
            case "S": // Subtraction
                System.out.printf("Result of %s subtract %s is %s %n", firstNum, secondNum, (firstNum - secondNum));
                break;

            // Exercise Answer
            // ------ Start ---------
            case "M": // Multiplication
                System.out.printf("Result of %s multiple %s is %s %n", firstNum, secondNum, (firstNum * secondNum));
                break;
            case "D": // Division
                System.out.printf("Result of %s divide %s is %s %n", firstNum, secondNum, (firstNum / secondNum));
                break;
            // ------ End ---------

            default:
                System.out.printf("Invalid operator: %s %n", operator);
        }
    }
}
